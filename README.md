# Escritura y depósito de la tesis

En este documento se enumeran una serie de detalles a tener en cuenta en el
proceso de escritura y depósito de una tesis por compendio de publicaciones. Es
importante leer este documento antes de comenzar con la escritura para evitar
errores de formato o tener que adaptar archivos posteriormente.

## Preparativos

Lo primero que se debe hacer es obtener una copia local de este repositorio. Se
recomiendan dos opciones, hacer un fork del repositorio y usar git como control
de versiones para la escritura de la tesis o, descargar el repositorio, eliminar
los archivos referentes a git (.git/ y .gitignore) y transaladar esta copia a
una localización de preferencia (muy recomendable trabajar con sincronización en
la nube: drive, icloud...). Cuando lo tengas listo, revisa el contenido de todas
las carpetas y archivos y asegúrate de que entiendes perfectamente la estructura
y función de todos los documentos.

__IMPORTANTE__: Asegúrate de haber leído y descargado la última versión de los
archivos necesarios proporcionados por la USC en la sección de _La tesis_ en
[esta dirección](https://www.usc.gal/es/centro/escuela-doctorado-internacional-usc)
(revisa todas las subsecciones).

Necesitarás además los siguientes documentos:

1. Para cada artículo:
   - La versión publicada por la revista.
   - La información referente a la publicación (año, volumen, pág., autores...)
     y también el índice de impacto de la revista en el año de la publicación.
     Este se puede consultar en el [Journal of Citation Reports](http://jcr-incites.fecyt.es/).
   - Se tienen que incluir las contribuciones específicas del autor de la tesis
      a esa publicación según la convención [CRediT](https://www.elsevier.com/authors/policies-and-guidelines/credit-author-statement).
   - Todos los archivos para la compilación de cada artículo: .tex, .bib,
     inputs, figuras.
   - Los pdf de la suplementary information en caso de haberla.
2. Permisos:
   - Permiso de coautores. Revisar la normativa y los documentos necesarios en
     la USC. Actualmente exigen varios documentos: Autorización de coautores, en el
     cual has de anexar (como pdfs independientes) la cesión firmada por cada coautor de cada publicación;
     declaración responsable, en la cual tú declaras que los autores ahí
     incluidos te han dado permiso (solo cuando no sea posible obtener la firma
     electrónica de los coautores); otra declaración responsable en caso de que
     no haya coautores de tipo doctor o no doctor; y por último quizá una declaración
     reponsable que has de crear tú en la que "des validez" a las firmas manuscritas
     que seguramente te envien los coautores extranjeros (no suelen tener certificados
     digitales).
3. Si optas a mención internacional:
   - La evaluación favorable de dos expertos internacionales. La plantilla
     también se descarga desde la página de la EDIUS y es recomendable enviarlo
     tan pronto se tenga una versión medianamente completa de la tesis (aunque
     falte alguna corrección menor) para asegurarse de tener los documentos
     antes de proceder con el depósito.
   - El documento de certificación de la estancia firmado por el centro receptor.
   - El documento de autorización de la estancia firmado por la CAPD.
4. Portada y contraportada: Estas se obtienen (tanto la versión digital como
   para la impresión) en [esta dirección](https://aplicacions.usc.es/axp/publica/portadatese/paso1). Para
   generarlas necesitas, además de otros datos triviales, un resumen que no debe
   exceder los 830 caracteres e irá impreso en la contraportada. Este resumen te
   podría servir también para cuando hagas la solicitud de depósito así que
   conviene tenerlo preparado. Si quedase mal formateado el pdf, puedes editarlo
   con Acrobat Pro siempre que no cambies el espíritu del diseño (puedes justificar
   texto, corregir lineas mal cortadas, etc).

## Plan de investigación

Asegúrate de tener todas las actividades (publicaciones, congresos, etc.)
incluidas en el documento de actividades de tu secretaría virtual y que estas
han sido aceptadas por tu director de tesis. Además, el idioma de presentación
de la tesis debe coincidir con el del plan de investigación en la sede virtual.
Si optas a mención internacional, recomendable hacerlo todo en inglés. Esto
puede suponer que tengas que hacer una traducción de tu plan de investigación el
cual tiene que ser aprobado por la CAPD antes del momento del depósito por tanto
asegúrate de que lo haces con tiempo.

## Escritura de la tesis

Cada uno escribirá la tesis como crea oportuno pero asegúrate de que incluyes lo
siguiente:

1. Resumen de la tesis de más de 3000 palabras en gallego e inglés al optar a
   mención internacional y, opcional, en castellano.
2. Que en los agradecimientos incluyes a las entidades que han ayudado al
   desarrollo de tu tesis (Ministerio, CESGA, proyectos...).
3. No es obligatorio pero, además del listado de publicaciones incluidas en la
   tesis, puedes meter, en la sección de "List of publications related to this
   thesis" aquellas en las que has contribuido pero no están recogidas en el
   documento.
4. Introducción: Obligatorio meter una sección de motivación y propósito y
   también otra de objetivos generales y específicos, donde además se detalle en
   qué capítulos se trata cada uno de los específicos. Luego puedes meter los
   apartados que consideres.
5. Métodos: también es obligatoria esta sección pero de contenido libre (siempre
   que explique las técnicas que has usado durante la tesis).
6. Resumen de los resultados: Una introducción en la parte de resultados de al
   menos 5000 palabras donde contextualices y des sentido a la tesis como un
   todo.
7. Introducción (de extensión libre) a cada artículo: Puedes inspirarte en los
   abstract de los artículos, ya que estos no aparecen incluidos a continuación
   (solo en la versión post-print en el apéndice).
8. Conclusiones: En esta parte debes incluir una sección con las conclusiones
   las cuales se leerán literalmente al final de la presentación en la defensa.
   También tienes que incluir otra sección de perspectivas futuras con las
   líneas que tratarás en el futuro (continuidad del trabajo).
9. Apéndices: La suplementary information de los artículos se recopilará en un
   apéndice después de las conclusiones. En cuanto a la versión publicada de los
   artículos, estos pueden ir tanto en un apéndice como incluidos en sus
   respectivas secciones, a gusto del autor. También puedes incluir más anexos con,
   por ejemplo, la información pertinente de cada publicación.
10. Bibliografía: Es recomendable revisar que no se cuelen errores o
    repeticiones.

## Modificación de los artículos para su inclusión en la tesis

Aunque no se puede cambiar el contenido de las publicaciones sí que debes
asegurarte de que todo aparece bien formateado cuando las incluyas en la tesis.
Afortunadamente, el script que puedes encontrar en `./scripts/fix_inputs.py` se
encarga de automatizar gran parte de los cambios de estilo y formato que
necesitas hacer. Lee muy atentamente el encabezado del script para ver cómo
ejecutarlo y qué es lo que hace, así como el cuerpo del script. Hasta que no
entiendas cómo funciona y lo que hace cada línea, no lo ejecutes. En su versión
actual está pensado para no sobreescribir archivos y por tanto no perder nada,
pero aun así, no lo ejecutes sin entender lo que hace. La ejecución del script
puede tardar en torno a 30 segundos o más en función de la complejidad de las
publicaciones y sus referencias. Cuando acaba su ejecución genera versiones
modificadas de los .tex que hayas incluido para su procesamiento y una versión
final de la bibliografía con todas las entradas encontradas. Asegúrate que en el
documento principal de la tesis estás incluyendo estos archivos modificados.

Hay otro script en (`./scripts/fix_spaces.py`) que está pensado para ejecutarse
una única vez y este sí que sobreescribe los archivos que en él incluyes. Por
tanto, recomiendo que antes de ejecutarlo hagas una copia de seguridad (la cual
puedes borrar una vez te asegures de que no se ha roto nada). Este script se
encarga de revisar los espacios después de las abreviaturas y otras
modificaciones menores (leer tanto el encabezado como el cuerpo del script antes
de ejecutarlo).

## Estilo de la tesis

Tradicionalmente la tesis se venia haciendo en formato libro (17x24cm), pero la
USC ha cambiado recientemente sus requisitos y ahora piden formato A4. Por si
se quisiera imprimir la tesis en el formato tradicional, se incluyen algunos
archivos extra que permiten la compilación de la tesis en ese formato (los que
incluyen "book_style" en el nombre). Además del propio tamaño de la página, se
incluyen automáticamente páginas en blanco para que cada sección empieze en la
página del lado derecho (impar).

## Entorno de desarrollo

Se recomienda encarecidamente utilizar Visual Studio Code para la escritura de
la tesis. Principalmente porque hace muy sencilla la compilación y el trabajo
con archivos LaTeX con la extensión "LaTeX Workshop". Además, la herramienta de
VSCode para buscar y reemplazar en todo el proyecto es de gran utilidad, como se
verá en el siguiente apartado donde se enumeran algunos cambios globales que
pueden ser necesarios.

Una opción muy útil para mantener limpios de archivos auxiliares de latex los
directorios de trabajo es cambiar el directorio de salida de los comandos de
latex. Para eso, en la configuración de VSCode, buscamos la opción `Latex: Out Dir`
dentro de la sección `Latex-workshop` y le asignamos el valor `%DIR%`. Con esto
conseguimos que, cuando compilemos, todos los archivo generados se guarden en la
carpeta `compile/` dentro de la carpeta donde esté el archivo principal de latex
(`thesis.tex`).

## Comandos y búsquedas de interés

### Páginas a color o en blanco y negro

Cuando se proceda a la impresión de la tesis, es posible que se requiera el
número de páginas que tienen contenido en color. Para averiguar qué páginas
están a color y cuáles en blanco y negro, se pueden ejecutar los siguientes
comandos respectivamente.

    ```
    gs -o - -sDEVICE=inkcov compile/thesis.pdf | grep -E "^\s.*" | awk '$1 != "0.00000" && $2 != "0.00000" && $3 != "0.00000" {print NR}' | paste -sd "," -
    gs -o - -sDEVICE=inkcov compile/thesis.pdf | grep -E "^\s.*" | awk '$1 == "0.00000" && $2 == "0.00000" && $3 == "0.00000" {print NR}' | paste -sd "," -
    ```

Esto devuelve una lista con los números de página con algo a color separados por
comas. Si lo que interesa es el número total de páginas de cada tipo se puede
ejecutar:

    ```
    gs -o - -sDEVICE=inkcov compile/thesis.pdf | grep -E "^\s.*" | awk '$1 != "0.00000" && $2 != "0.00000" && $3 != "0.00000" {print NR}' | wc -l
    gs -o - -sDEVICE=inkcov compile/thesis.pdf | grep -E "^\s.*" | awk '$1 == "0.00000" && $2 == "0.00000" && $3 == "0.00000" {print NR}' | wc -l
    ```

Para que estos comandos funcionen hay que tener instalado
[Ghostscript](https://www.ghostscript.com/doc/current/Install.htm).

### Revisión de formatos

Con la herramienta de búsqueda global de VSCode (la lupa de la barra lateral) se
pueden hacer búsquedas usando regular expressions y excluyendo archivos que
pueden ser muy útiles para comprobar que no se nos escapan errores de formatos
sobre todo. Además, nos permite reemplazar los resultados usando haciendo
capturas con las regex que también puede ser útil.

__NOTA__: En VSCode los grupos capturados se referencian con el símbolo $ ($1, $2,...).

Si se incluyen en la búsqueda únicamente los archivos latex con la regex
`**/*.tex`, algunas búsquedas útiles pueden ser:

- Para asegurarse de que las cosas están referenciadas con \ref{} y no con
  números (que no se haya colado una referencia sin usar labels):

    ```
    (Ref|Fig|Eq)s?\.\\[ \n][0-9]
    ```

- Para asegurarse de poner \ después del punto de las abreviaciones y que así
  aparezca un espacio normal:

    ```
    (Ref|Fig|Eq|i\.e|e\.g|cf| ca)s?\.[ \n]
    ```

- Asegurase de que después de un Ref. viene un citenum y no un cite:

    ```
    Refs?\.\\[ \n]\\cite\{
    ```

## Proceso de solicitud de depósito

Si has seguido todos los pasos anteriores y has recopilado toda la documentación
necesaria ya puedes proceder con la solicitud de depósito de la tesis. Revisa en
la página de la EDIUS el link para proceder con el depósito. Además de los
documentos recopilados necesitarás un resumen breve (podría valer el de la
contraportada de la tesis) y 3 códigos UNESCO en los cuales encaja la temática
de la tesis. Estos pueden ser consultados
[aquí](https://pro-assets-usc.azureedge.net/cdn/ff/2EmXKsaMkk8z8pGoA7WDGglpjAi51wB86LYVaOOZKeM/1645533033/public/documents/2022-02/codigos_unesco.pdf).
Con todo eso deberías poder completar el formulario de la solicitud. No te
sorprendas si en más de un campo tienes que subir el mismo archivo (por ejemplo
la autorización del director de la tesis puede ser la misma que la del tutor de
la tesis).

Hecha la solicitud, en los próximos días se pondrán en contacto contigo y te
solicitarán:

1. Los ejemplares con y sin artículos de la tesis. Comprueba que has metido la
   portada y contraportada.
2. Que cubras un documento Word con la información de los miembros que propones
   para el tribunal (presidente, vocal, secretario, suplentes para cada puesto y
   otros 2 suplentes a mayores). Asegúrate de que tu selección cumple con los
   requisitos. Por ejemplo, si optas a mención internacional, ha de haber un
   miembro de una entidad extranjera. Los requisitos están en la web de la
   EDIUS.
3. La conformidad de formar parte del tribunal firmada por cada miembro.
4. Fecha tentativa para la defensa de la tesis.
5. Centro de adscripción.

__IMPORTANTE__: Para la estimación de la fecha de defensa ten muy en cuenta los
plazos. Desde que envías estos documentos, han de recopilarse y convocar una
reunión de la CAPD para que evalúe la corrección formal de la tesis. Si se
supera esta evaluación, la tesis debe quedar en depósito un plazo de 10 días lectivos
(sin contar fines de semana ni festivos). Pasados estos días, la tesis será
evaluada por la EDIUS en la reunión programada más próxima (consultar en el
correo de envío de esta documentación). Si la aceptan, la fecha de defensa será
comunicada con 7 días de antelación.
