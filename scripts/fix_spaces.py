"""
Script para arreglar que los espacios después de las abreviaciones (Ref., Fig.,
eq...) sean considerados espacios normales y no como puntos seguidos. También
otra serie de cambios.
"""

import re
import difflib

files = [
    "../frontmatter/abstract.tex",
    "../frontmatter/resumen.tex",
    "../frontmatter/resumo.tex",
    "../mainmatter/appendix.tex",
    # Añade todos los archivos que quieras arreglar
]


changes = 0
for fname in files:
    with open(fname, 'r') as f:
        content = f.readlines()
        
    content_mod = []
    for line in content:
        # Añade espacios normales después de las abreviaciones
        new_line = re.sub(r'([\ \n])((Figs?|[Ee]qs?|i\.e|e\.g|Refs?|ca|cf))\.([\ \n])', r'\1\3.\\\4', line)
        # Reemplaza force-field por force field si no está dentro de un \ref o \label
        if re.search(r'force\-field', new_line):
            if not re.search(r'\\(ref|label)', new_line):
                new_line = re.sub(r'force\-?field', r'force field', new_line)
        # Se asegura que las refs a ecuaciones estén rodeadas por parentesis:
        # Eq. 1 -> Eq. (1)
        new_line = re.sub(r'((\ |^))(\\ref\{eq:.*?\})', r'\2(\3)', line)
        content_mod.append(new_line)

    # Se imprimen las líneas cambiadas
    diff_lines = difflib.unified_diff(
        content, content_mod, n=0,
    )
    print('\033[92m' + fname + '\033[0m')
    for line in diff_lines:
        changes += 1
        print(line)
    with open(fname, 'w') as f:
        f.writelines(content_mod)

# Se imprime el número de cambios
print(changes)
