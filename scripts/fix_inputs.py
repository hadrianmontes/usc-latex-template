r"""
Script para arreglar los .tex y la bibliografía de los diferentes artículos a
incluir en la tesis.

NOTA: Se requiere python 3.8 o superior


¿Qué es lo que hace?
--------------------

El script importa las clases BibFile y LatexFile de un repositorio de GitHub
del que se ha de tener una copia en local. La url del repositorio es:

    https://github.com/txemaotero/unify_bibliography

También se necesita otro repositorio para comprobar que los nombres de las
revistas están abreviadas (o si no lo están cambiarlas). La url sería:

    https://github.com/txemaotero/cornvert_journal_abr

Las rutas a las copias locales de estos repositorios se incluyen en el path
para poder hacer los imports en la primera parte del script. Ahora mismo está
pensado para que estos repositorios estén dentro de la carpeta "scripts", si
los tienes en otro lugar debes modificar el principio de este script.

La clase BibFile permite cargar todas las bibliografías usadas en todos los
latex a incluir en la tesis y juntarlas. Una vez combinadas (donde se comprueba
que no haya entradas con las mimas claves) se comprueba que no haya entradas
repetidas comprobando los títulos, doi e issn. Si se encuentra alguna
entrada repetida se combinan para generar una entrada con los campos definidos
en los dos y se hace un registro de cómo se habrían de cambiar las claves
bibliográficas. 

Hecho esto, se lee cada latex a incluir en la tesis y se hacen una serie de
modificaciones:
    - Se reemplazan los \input{} por el contenido de los archivos que incluyen.
    - Se modifican los \label y \ref para incluir una etiqueta identificadora y
      así evitar que haya figuras o ecuaciones que repitan etiquetas.
    - Se arreglan las rutas parciales que haya de figuras y de más para que
      estén referidas a la ruta del directorio de la tesis.
    - Se eliminan las partes que no deben estar en el documento para la tesis
      (cabeceras, abstract, imports, etc.), quedando sólo el documento
      principal (las secciones).
    - Se verifica que las secciones Acknoledgements y Conflict of Interests no
      sean numeradas.
    - Se modifican las citas para que tengan en cuenta los cambios hechos
      después de juntar todas las bibliografías.
    - Se comprueba que las citas sigan el formatao adecuado (que estas se hagan
      despúes de los signos de puntuación) y que si hay un \cite después
      de un Ref. se cambie a \citenum.

Al final se imprimen por pantalla las lineas necesarias para incluir en cada
artículo en la tesis (en el results.tex) aunque habría que revisar que los
campos sean correctos y el orden de los artículos. También se imprimen todos
los paquetes diferentes que se usan en todos los latex por si hubiera que
incluirlos en el documento principal.

OJO: si alguna cita de un .bib tiene un signo "=" en algún campo como el título,
el script no funciona bien y debes corregir manualmente el problema. Un ejemplo
de cita que daría problemas podría ser:

@article{authornameYEAR,
  title={A wonderful title about how x = y},
  author={Surname, Name and Surname, Name},
  journal={Journal Name},
  volume={XX},
  number={XX},
  pages={XXXX},
  year={XXXX},
  publisher={Publisher Name}
}


Preparación
-----------

Para usar este script lo primero que hay que hacer es copiar los archivos
necesarios para compilar cada artículo en su correspondiente carpeta. Esto
incluiría las figuras, el .tex principal, los inputs en caso de haberlos y
el/los .bib. Esto ha de copiarse en la carpeta:

    ../mainmatter/article_source/etiqueta_articulo/ 
    
Donde etiqueta_articulo será un identificador del artículo y que se usará tanto
para singularizar los labels y refs, como para el nombre de los pdfs a incluir
de cada artículo y los .tex dentro de la carpeta article_intro. Para poner un
ejemplo, supongamos que la tesis consta de dos artículos, uno de ellos trata de
solvatación en LIs y otro de dinámica. Un ejemplo de esta estructura de
carpetas sería:

mainmatter/
    article_source/
        il_solvatation/
            manuscript.tex
            biblio.bib
            bibliography.bib
            figs/
                fig1.pdf
                fig2.pdf
            inputs/
                input1.tex
                input2.tex
        dynamics/
            manuscript.tex
            biblio.bib
            figures/
                fig1.pdf
    article_pdf/
        il_solvatation.pdf
        dynamics.pdf
    article_intro/
        il_solvatation.tex
        dynamics.tex

IMPORTANTE: Aunque se podría cambiar, los .tex de los artículos se tienen que
llamar "manuscript.tex". Los bib, inputs y figuras dan igual cómo se llamen
siempre y cuando las rutas dentro del latex estén bien (por ejemplo, dentro de
il_solvation/manuscript.tex la figura 1 se incluiría como ./figs/fig1.pdf).


Ejecutando el script
--------------------

Con estos preparativos listos, faltaría modificar las primeras líneas del
script para que ROOT_DIR sea la ruta a la carpeta de la tesis (donde se
encuentra la plantilla) y que los paths a los repositorios de
github mencionado antes estén bien. Con esto podemos ejecutar el script:

    python3 fix_inputs.py

Esto imprimirá por pantalla los cambios que se hacen a las entradas de la
bibliografía que se repiten. Después, se imprimen las líneas que se han de
pegar en el results.tex para incluir los artículos. Finalmente, se imprimen los
paquetes que se usan en todos los latex por si hay que incluirlos en el
documento principal (thesis.tex). Al acabar, el script generará un archivo
"bibliography_merged.bib" en la raíz del proyecto que contiene todas las
bibliografías ya sin entradas repetidas. Este será el archivo que se ha de
incluir en thesis.tex. Para cada manuscript.tex de cada artículo se generará un
manuscript_mod.tex con los cambios pertinentes para que se incluya en el
documento principal.


Recomendaciones
---------------

A la hora de hacer modificaciones en los tex de cada artículo, es recomendable
hacerlos sobre los manuscript.tex y no sobre los manuscript_mod.tex. Así, en
caso de que encontremos que hay que hacer modificaciones generales sobre todos
los .tex que se puedan automatizar modificando este script, no perderemos los
cambios al ejecutar de nuevo el script. Eso sí, tras cada cambio que hagamos en
el manuscript.tex, hay que ejecutar de nuevo el script para que se generen los
manuscript_mod.tex y que al compilar la tesis se incluyan los cambios
pertinentes.

"""

from glob import glob
import os
import sys

ROOT_DIR = "../"

#### Añadir los paths para poder importar
# Path al repositorio con las herramientas para modificar tex y bib
sys.path.insert(1, os.path.join(ROOT_DIR, "scripts/unify_bibliography"))
# Incluir al path el repositorio para las abreviaciones
sys.path.insert(1, os.path.join(ROOT_DIR, "scripts/cornvert_journal_abr"))

from parsers import BibFile, LatexFile
import abbreviation


articles_source_dirs = glob(os.path.join(ROOT_DIR, "mainmatter", "article_source", "*"))
articles_labels = [os.path.split(i)[-1] for i in articles_source_dirs]

# Encontrar todas las entradas de biblio, fusionarlas y eliminar duplicados.
# Se generan archivos intermedios para estar seguro de que no se elimina nada
print("Generanting total bibliography...")
total_bib = sum(
    [
        BibFile(fbib)
        for dir_name in articles_source_dirs
        for fbib in glob(os.path.join(dir_name, "*.bib"))
    ],
    BibFile(os.path.join(ROOT_DIR, "./bibliography_additional.bib")),
)
print("Merging duplicates")
merged_dict = total_bib.merge_duplicated_entries()
print("Writing total bibliography")
total_bib.write(os.path.join(ROOT_DIR, "bibliography_merged_long.bib"))
print("Checking journal abbreviations")
abbreviation.convert_bib(
    os.path.join(ROOT_DIR, "bibliography_merged_long.bib"),
    os.path.join(ROOT_DIR, "bibliography_merged.bib"),
)
print("Abbreviations checked")


print("Modifying Latex files...")
# Se revisan las citas de ciertos archivos por si se han fusionado y tienen otros keys
for fname in ("methods.tex", "introduction.tex", "introduction_to_results.tex"):
    latex_file = LatexFile(os.path.join(ROOT_DIR, "mainmatter", fname))
    latex_file.replace_cite_entries(merged_dict)
    latex_file.write(os.path.join(ROOT_DIR, "mainmatter", "mod_" + fname))

# Lee los latex de cada artículo y los limpia para y modifica para incluirlos en la tesis
results_lines = []
used_packages: dict[str, str] = {}
for label, dir_name in zip(articles_labels, articles_source_dirs):
    print("Modifying", label)
    latex_file = LatexFile(os.path.join(dir_name, "manuscript.tex"))
    latex_file.substitute_inputs()
    latex_file.fix_labels_refs()
    latex_file.fix_partial_paths()
    latex_file.extract_sections()
    latex_file.replace_cite_entries(merged_dict)
    latex_file.adapt_citations()
    print("Writing", label)
    latex_file.write(os.path.join(dir_name, "manuscript_mod.tex"))
    results_lines.append(latex_file.lines_for_results())
    used_packages = {**used_packages, **latex_file.packages}

print("Lines to include articles in results:\n\n")
print("\n".join(results_lines))

print("\nThe different packages used in all the latex files are:\n\n")
for package, version in used_packages.items():
    print(version, "\t%", package)
